<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Calculo nomina</title>
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>

 <h2>Calculo para: </h2>
 <p>${pago.toString()}</p>
  
 <div class="container">  
  <div class="starter-template">
   <h2>Conceptos de pago</h2>
	<table class="table table-striped table-hover table-condensed table-bordered">
    	<tr>
     		<th>Tipo</th>
     		<th>Concepto</th>
     		<th>Monto</th>
    	</tr>
		<c:forEach var="cpto" items="${pago.getConceptosPagados()}">
		   <tr>
				<td>${cpto.tipo}</td>
      			<td>${cpto.cpto}</td>
      			<td>${cpto.monto}</td>
      	   </tr>		
		</c:forEach>
	</table>
  </div>
 </div> 
</body>
</html>