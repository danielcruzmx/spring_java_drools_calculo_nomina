<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulario de busqueda</title>
</head>
<body>

	<h2>Calculo Nomina</h2>
	<h2>Búsqueda de empleados</h2>

	<form:form action="procesarCriterio" modelAttribute="criterio"> 
		
		RFC: <form:input path="rfcEmpleado"/>
		<br><br><br>
		Puesto: <form:input path="idPuestoNomb"/>
		<br><br><br>
		Unidad: <form:input path="idUnidadNomb"/>
		<br><br><br>
		<input type="submit" value="Enviar">

	</form:form>

</body>
</html>