<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista de Empleados</title>
<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
	
	<div class="container">
	  <div class="starter-template">
   		<h2>Lista Empleados</h2>
   			<table class="table table-striped table-hover table-condensed table-bordered">
    			<tr>
     				<th>RFC</th>
     				<th>Plaza</th>
     				<th>Unidad</th>
     				<th>Grupo</th>
     				<th>Puesto</th>
     				<th>Calculo</th>
    			</tr>
    			<c:forEach var="plaza" items="${empleados}">
     				<tr>
      					<td>${plaza.rfcEmpleado}</td>
      					<td>${plaza.idPlaza}</td>
      					<td>${plaza.idUnidadNomb}</td>
      					<td>${plaza.idGrupoPago}</td>
      					<td>${plaza.idPuestoNomb}</td>
      					<td><a href="muestraCalculo?empleado=${plaza.rfcEmpleado}">Calcula</a></td>
     				</tr>
    			</c:forEach>
   			</table>
  		</div>
	 </div>



</body>
</html>