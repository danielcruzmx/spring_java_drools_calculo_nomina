package com.danielcruzmx.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "td_empleado")
public class Empleado {

	@Column(name="RFC_EMPLEADO")
    @Id
    private String rfcEmpleado;
	
	@Column(name="ID_ULTIMO_MOVTO")
    private String idUltimoMovimiento;
	
	@Column(name="PRIMER_APELLIDO")
    private String primerApellido;
    
    @Column(name="SEGUNDO_APELLIDO")
    private String segundoApellido;
    
    @Column(name="NOMBRE_EMPLEADO")
    private String nombreEmpleado;
    
    @Column(name="ID_TIPO_PAGO")
    private String idTipoPago;
    
    @Column(name="FEC_ULTIMO_MOVTO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private java.util.Date fecUltimoMovto;
    
    @Column(name="RFC_UNICO")
    private String rfcUnico;

	public String getRfcEmpleado() {
		return rfcEmpleado;
	}

	public void setRfcEmpleado(String rfcEmpleado) {
		this.rfcEmpleado = rfcEmpleado;
	}

	public String getIdUltimoMovimiento() {
		return idUltimoMovimiento;
	}

	public void setIdUltimoMovimiento(String idUltimoMovimiento) {
		this.idUltimoMovimiento = idUltimoMovimiento;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	public String getIdTipoPago() {
		return idTipoPago;
	}

	public void setIdTipoPago(String idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	public java.util.Date getFecUltimoMovto() {
		return fecUltimoMovto;
	}

	public void setFecUltimoMovto(java.util.Date fecUltimoMovto) {
		this.fecUltimoMovto = fecUltimoMovto;
	}

	public String getRfcUnico() {
		return rfcUnico;
	}

	public void setRfcUnico(String rfcUnico) {
		this.rfcUnico = rfcUnico;
	}
	
	
    
}
