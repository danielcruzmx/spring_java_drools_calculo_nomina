package com.danielcruzmx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "td_pension")
public class Pension implements Serializable {
		private static final long serialVersionUID = 1L;

		@Column(name="ID_PENSION")    
		@Id
		private java.lang.Long idPension;
		
		@Column(name="RFC_PENSION")
	    private java.lang.String rfcPension;
		
		@Column(name="NUM_PENSION")
	    private int numPension;
		
		@Column(name="STATUS_PENSION")
		private String statusPension;
		
		@Column(name="BENEFICIARIA_PENSION")
	    private java.lang.String beneficiariaPension;
		
		@Column(name="MONTO_PENSION")
		private java.lang.Double montoPension;
		
		@Column(name="PORCENTAJE_PENSION")
		private java.lang.Float porcentajePension;
		
		@Column(name="MONTO_ADEUDO")
		private java.lang.Double montoAdeudo;
		
		@Column(name="QNA_APLICA_ADEUDO")
	    private java.lang.Integer qnaAplicaAdeudo;
		
		@Column(name="QNA_DESC_ADEUDO")
		private java.lang.Integer qnaDescAdeudo;
		
		@Column(name="CPTOS_PORCENTAJE")
		private java.lang.String cptosPorcentaje;
	    
		public Pension(){
		}
	    
		public java.lang.Long getIdPension() {
			return idPension;
		}
		public void setIdPension(java.lang.Long idPension) {
			this.idPension = idPension;
		}
		public java.lang.String getRfcPension() {
			return rfcPension;
		}
		public void setRfcPension(java.lang.String rfcPension) {
			this.rfcPension = rfcPension;
		}
		public int getNumPension() {
			return numPension;
		}
		public void setNumPension(int numPension) {
			this.numPension = numPension;
		}
		public java.lang.String getBeneficiariaPension() {
			return beneficiariaPension;
		}
		public void setBeneficiariaPension(java.lang.String beneficiariaPension) {
			this.beneficiariaPension = beneficiariaPension;
		}
		public java.lang.Double getMontoPension() {
			return montoPension;
		}
		public void setMontoPension(java.lang.Double montoPension) {
			this.montoPension = montoPension;
		}
		public java.lang.Float getPorcentajePension() {
			return porcentajePension;
		}
		public void setPorcentajePension(java.lang.Float porcentajePension) {
			this.porcentajePension = porcentajePension;
		}
		public java.lang.Double getMontoAdeudo() {
			return montoAdeudo;
		}
		public void setMontoAdeudo(java.lang.Double montoAdeudo) {
			this.montoAdeudo = montoAdeudo;
		}
		public java.lang.Integer getQnaDescAdeudo() {
			return qnaDescAdeudo;
		}
		public void setQnaDescAdeudo(java.lang.Integer qnaDescAdeudo) {
			this.qnaDescAdeudo = qnaDescAdeudo;
		}
		public java.lang.Integer getQnaAplicaAdeudo() {
			return qnaAplicaAdeudo;
		}
		public void setQnaAplicaAdeudo(java.lang.Integer qnaAplicaAdeudo) {
			this.qnaAplicaAdeudo = qnaAplicaAdeudo;
		}
		public java.lang.String getCptosPorcentaje() {
			return cptosPorcentaje;
		}
		public void setCptosPorcentaje(java.lang.String cptosPorcentaje) {
			this.cptosPorcentaje = cptosPorcentaje;
		}

		public String getStatusPension() {
			return statusPension;
		}

		public void setStatusPension(String statusPension) {
			this.statusPension = statusPension;
		}
}

