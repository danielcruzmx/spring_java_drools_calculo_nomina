package com.danielcruzmx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Temporal;

@NamedNativeQuery( name="Pago.findByRfcEmpleado",query = "" + 
"select td_empleado.RFC_EMPLEADO, " +
"	    td_empleado.NOMBRE_EMPLEADO, " +
"	    td_empleado.PRIMER_APELLIDO, " +
"	    td_empleado.SEGUNDO_APELLIDO, " +
"		td_empleado.COMPAT_EMPLEO, " +
"       td_plaza.ID_PLAZA, " +
"       td_plaza.ID_UNIDAD_NOM, " +
"       td_plaza.ID_GRUPO_PAGO, " +
"		td_plaza.ID_ZONA_DIST_NOM, " +
"		td_plaza.PLAZA_IMP_HONO, " +
"		td_plaza.PLAZA_QNA_CAPTURA, " +
"		td_plaza.PLAZA_ULTIMO_MOVTO, " +
"		td_plaza.ID_MOVTO_PERSONA, " +
"		tc_grupo_pago.DESC_GRUPO_PAGO, " +
"       tc_atributo_puesto_nom.ID_NIVEL_PTO, " +
"       tc_atributo_puesto_pre.ID_NIVEL_PTO as ID_NIVEL_PRE, " +
"       tc_atributo_puesto_nom.ID_NOMBRAMIENTO, " +
"       tc_atributo_puesto_nom.ID_JERARQUIA, " +
"		tc_atributo_puesto_nom.AP_ERARIO_FEDERAL, " +
"		tc_atributo_puesto_nom.AP_INCENTIVO, " +
"	    tc_nombramiento.DESC_NOMBRAMIENTO, " +
"	    tc_jerarquia.DESC_JERARQUIA, " +
"       tc_tabulador_nom.TAB_SUELDO, " +
"       tc_tabulador_nom.TAB_COMPENSACION, " +
"       tc_tabulador_pre.TAB_SUELDO AS TAB_SUELDO_PRE, " +
"       tc_tipo_pago.DESC_TIPO_PAGO    " + 
"from   td_plaza, " +
"       td_empleado, "  +
"       tc_atributo_puesto tc_atributo_puesto_nom, " +
"       tc_tabulador       tc_tabulador_nom, " +
"       tc_atributo_puesto tc_atributo_puesto_pre, " +
"       tc_tabulador       tc_tabulador_pre, " +
"		tc_grupo_pago, " +
"		tc_nombramiento, " +
"		tc_jerarquia, " + 
"		tc_tipo_pago " + 
"where  td_plaza.PLAZA_RFC                  = td_empleado.RFC_EMPLEADO " +
"and    td_plaza.ID_PUESTO_NOM              = tc_atributo_puesto_nom.ID_ATRIBUTO_PUESTO " +
"and    tc_atributo_puesto_nom.AP_FIN       = to_date('01/01/2099','dd/mm/yyyy') " +
"and    tc_tabulador_nom.TAB_FIN            = to_date('01/01/2099','dd/mm/yyyy') " +
"and    tc_atributo_puesto_nom.ID_NIVEL_PTO = tc_tabulador_nom.ID_NIVEL_PTO " +
"and    td_plaza.ID_RANGO_NOM               = tc_tabulador_nom.ID_RANGO " +
"and    td_plaza.ID_ZONA_ECO_NOM            = tc_tabulador_nom.ID_ZONA_ECON " +
"and    td_plaza.ID_PUESTO_PRE              = tc_atributo_puesto_pre.ID_ATRIBUTO_PUESTO " +
"and    tc_atributo_puesto_pre.AP_FIN       = to_date('01/01/2099','dd/mm/yyyy') " +
"and    tc_tabulador_pre.TAB_FIN            = to_date('01/01/2099','dd/mm/yyyy') " +
"and    tc_atributo_puesto_pre.ID_NIVEL_PTO = tc_tabulador_pre.ID_NIVEL_PTO " +
"and    td_plaza.ID_RANGO_PRE               = tc_tabulador_pre.ID_RANGO " +
"and    td_plaza.ID_ZONA_ECO_PRE            = tc_tabulador_pre.ID_ZONA_ECON " +
"and    td_plaza.ID_SIT_PLAZA like 'O%' " +
"and    td_plaza.PLAZA_RFC like ?1 "  +
"and    td_plaza.ID_GRUPO_PAGO                 = tc_grupo_pago.ID_GRUPO_PAGO " +
"and    tc_atributo_puesto_nom.ID_NOMBRAMIENTO = tc_nombramiento.ID_NOMBRAMIENTO " +
"and    tc_atributo_puesto_nom.ID_JERARQUIA    = tc_jerarquia.ID_JERARQUIA " +
"and    td_empleado.ID_TIPO_PAGO               = tc_tipo_pago.ID_TIPO_PAGO ", resultClass=Pago.class)


@Entity
public class Pago implements Serializable{
	private static final long serialVersionUID = 1L;
	public final static String FIND_RFC = "Pago.findByRfcEmpleado";
    
    @Column(name="RFC_EMPLEADO")
    private String rfcEmpleado;
    
    @Column(name="PRIMER_APELLIDO")
    private String primerApellido;
    
    @Column(name="SEGUNDO_APELLIDO")
    private String segundoApellido;
    
    @Column(name="NOMBRE_EMPLEADO")
    private String nombreEmpleado;
            
  
	@Column(name="ID_PLAZA")
    @Id
    private Integer idPlaza;
    
    @Column(name="ID_UNIDAD_NOM")
    private Integer idUnidadNomb;
    
    @Column(name="ID_GRUPO_PAGO")
    private String idGrupoPago;  
    
    @Column(name="DESC_GRUPO_PAGO")
    private String descGrupoPago;
    
    @Column(name="ID_NIVEL_PTO")
    private String idNivelPto;
    
    @Column(name="ID_NOMBRAMIENTO")
    private String idNombramiento;
    
    @Column(name="DESC_NOMBRAMIENTO")
    private String descNombramiento;
    
    @Column(name="ID_JERARQUIA")
    private String idJerarquia; 
    
    @Column(name="DESC_JERARQUIA")
    private String descJerarquia;
    
    @Column(name="TAB_SUELDO")
    private double tabSueldo;
    
    @Column(name="TAB_COMPENSACION")
    private double tabCompensacion;
    
    @Column(name="AP_ERARIO_FEDERAL")
    private String erario; 
    
    @Column(name="PLAZA_IMP_HONO")
    private double impHonorario;
    
    @Column(name="TAB_SUELDO_PRE")
    private double tabSueldoPre;
    
    @Column(name="COMPAT_EMPLEO")
    private String cotiza;
    
    @Column(name="AP_INCENTIVO")
    private double incentivo;

    @Column(name="PLAZA_QNA_CAPTURA")
    private int quincenaMovimiento;
    
    @Column(name="ID_MOVTO_PERSONA")
    private int movimiento;
    
    @Column(name="PLAZA_ULTIMO_MOVTO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private java.util.Date fecUltimoMovto;
        	
    public Pago(){
    }

	public String getRfcEmpleado() {
		return rfcEmpleado;
	}

	public void setRfcEmpleado(String rfcEmpleado) {
		this.rfcEmpleado = rfcEmpleado;
	}

	public Integer getIdPlaza() {
		return idPlaza;
	}

	public void setIdPlaza(Integer idPlaza) {
		this.idPlaza = idPlaza;
	}

	public Integer getIdUnidadNomb() {
		return idUnidadNomb;
	}

	public void setIdUnidadNomb(Integer idUnidadNomb) {
		this.idUnidadNomb = idUnidadNomb;
	}

	public String getIdGrupoPago() {
		return idGrupoPago;
	}

	public void setIdGrupoPago(String idGrupoPago) {
		this.idGrupoPago = idGrupoPago;
	}

	public String getDescGrupoPago() {
		return descGrupoPago;
	}

	public void setDescGrupoPago(String descGrupoPago) {
		this.descGrupoPago = descGrupoPago;
	}

	public String getIdNivelPto() {
		return idNivelPto;
	}

	public void setIdNivelPto(String idNivelPto) {
		this.idNivelPto = idNivelPto;
	}

	public String getIdNombramiento() {
		return idNombramiento;
	}

	public void setIdNombramiento(String idNombramiento) {
		this.idNombramiento = idNombramiento;
	}

	public String getDescNombramiento() {
		return descNombramiento;
	}

	public void setDescNombramiento(String descNombramiento) {
		this.descNombramiento = descNombramiento;
	}

	public String getIdJerarquia() {
		return idJerarquia;
	}

	public void setIdJerarquia(String idJerarquia) {
		this.idJerarquia = idJerarquia;
	}

	public String getDescJerarquia() {
		return descJerarquia;
	}

	public void setDescJerarquia(String descJerarquia) {
		this.descJerarquia = descJerarquia;
	}

	public double getTabSueldo() {
		return tabSueldo;
	}

	public void setTabSueldo(double tabSueldo) {
		this.tabSueldo = tabSueldo;
	}

	public double getTabCompensacion() {
		return tabCompensacion;
	}

	public void setTabCompensacion(double tabCompensacion) {
		this.tabCompensacion = tabCompensacion;
	}

	public String getErario() {
		return erario;
	}

	public void setErario(String erario) {
		this.erario = erario;
	}

	public double getImpHonorario() {
		return impHonorario;
	}

	public void setImpHonorario(double impHonorario) {
		this.impHonorario = impHonorario;
	}

	public double getTabSueldoPre() {
		return tabSueldoPre;
	}

	public void setTabSueldoPre(double tabSueldoPre) {
		this.tabSueldoPre = tabSueldoPre;
	}

	public String getCotiza() {
		return cotiza;
	}

	public void setCotiza(String cotiza) {
		this.cotiza = cotiza;
	}

	public double getIncentivo() {
		return incentivo;
	}

	public void setIncentivo(double incentivo) {
		this.incentivo = incentivo;
	}

	public int getQuincenaMovimiento() {
		return quincenaMovimiento;
	}

	public void setQuincenaMovimiento(int quincenaMovimiento) {
		this.quincenaMovimiento = quincenaMovimiento;
	}

	public int getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(int movimiento) {
		this.movimiento = movimiento;
	}

	public java.util.Date getFecUltimoMovto() {
		return fecUltimoMovto;
	}

	public void setFecUltimoMovto(java.util.Date fecUltimoMovto) {
		this.fecUltimoMovto = fecUltimoMovto;
	}
	
	public String getNombreCompleto() {
		return primerApellido + " " + segundoApellido + " " + nombreEmpleado;
	}
	
	public String getPrimerApellido() {
			return primerApellido;
	}
	
	
	
	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	@Override
    public String toString() {
        return "Pago [RFC=" + rfcEmpleado + ", Nombre=" + primerApellido + " " + segundoApellido + " " + nombreEmpleado + ", Plaza=" + idPlaza.toString() + ", Nivel=" + idNivelPto + ", GrupoPago=" + descGrupoPago +
            ", Nombramiento=" + descNombramiento + ", Jerarquia=" + descJerarquia + ", Unidad=" + idUnidadNomb + "]";
    }

	
}
