package com.danielcruzmx.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tc_ispt")
@IdClass(TcIsptPk.class)
public class TcIspt implements Serializable {
	private static final long serialVersionUID = 1L;
	public final static String FIND_TIPO = "TcIspt.findCveTipoTabla";
   
	@Column(name="ispt_FIN")
	@Temporal(javax.persistence.TemporalType.DATE)
	@Id
	private Date fecFin;
	
	@Column(name="id_TIPO_TABLA")
	@Id
	private String  cveTipoTabla;
		
	@Column(name="ispt_CONSEC")
	@Id
	private Integer consecutivo;
	
	@Column(name="ispt_LIM_INF1")
    private double limiteInferior1;
	
	@Column(name="ispt_LIM_INF2")
    private double limiteInferior2;
	
	@Column(name="ispt_LIM_SUPERIOR")
    private double limiteSuperior;
	
	@Column(name="ispt_sueldo_BRUTO1")
    private double sueldoBruto1;
	
	@Column(name="ispt_SUELDO_BRUTO2")
    private double sueldoBruto2;
	
	@Column(name="ispt_CUOTA_FIJA")
    private double cuotaFija;
	
	@Column(name="ispt_EXCEDENTE")
    private double excedente;
	
	@Column(name="ispt_SUBSIDIO")
    private double subsidio;
	
	@Column(name="ispt_SUELDO_ANUAL")
    private double sueldoAnual;
	
	@Column(name="ispt_PORCENTAJE")
    private float porcentaje;
	
	@Column(name="ispt_CREDITO_SALARIO")
	private double creditoSalario;

	public Date getFecFin() {
		return fecFin;
	}

	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}

	public String getCveTipoTabla() {
		return cveTipoTabla;
	}

	public void setCveTipoTabla(String cveTipoTabla) {
		this.cveTipoTabla = cveTipoTabla;
	}

	public Integer getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}

	public double getLimiteInferior1() {
		return limiteInferior1;
	}

	public void setLimiteInferior1(double limiteInferior1) {
		this.limiteInferior1 = limiteInferior1;
	}

	public double getLimiteInferior2() {
		return limiteInferior2;
	}

	public void setLimiteInferior2(double limiteInferior2) {
		this.limiteInferior2 = limiteInferior2;
	}

	public double getLimiteSuperior() {
		return limiteSuperior;
	}

	public void setLimiteSuperior(double limiteSuperior) {
		this.limiteSuperior = limiteSuperior;
	}

	public double getSueldoBruto1() {
		return sueldoBruto1;
	}

	public void setSueldoBruto1(double sueldoBruto1) {
		this.sueldoBruto1 = sueldoBruto1;
	}

	public double getSueldoBruto2() {
		return sueldoBruto2;
	}

	public void setSueldoBruto2(double sueldoBruto2) {
		this.sueldoBruto2 = sueldoBruto2;
	}

	public double getCuotaFija() {
		return cuotaFija;
	}

	public void setCuotaFija(double cuotaFija) {
		this.cuotaFija = cuotaFija;
	}

	public double getExcedente() {
		return excedente;
	}

	public void setExcedente(double excedente) {
		this.excedente = excedente;
	}

	public double getSubsidio() {
		return subsidio;
	}

	public void setSubsidio(double subsidio) {
		this.subsidio = subsidio;
	}

	public double getSueldoAnual() {
		return sueldoAnual;
	}

	public void setSueldoAnual(double sueldoAnual) {
		this.sueldoAnual = sueldoAnual;
	}

	public float getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(float porcentaje) {
		this.porcentaje = porcentaje;
	}

	public double getCreditoSalario() {
		return creditoSalario;
	}

	public void setCreditoSalario(double creditoSalario) {
		this.creditoSalario = creditoSalario;
	}
}