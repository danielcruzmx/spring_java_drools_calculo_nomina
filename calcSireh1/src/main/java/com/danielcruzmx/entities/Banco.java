package com.danielcruzmx.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tc_banco")
public class Banco {
	
	@Id
	@Column(name = "ID_BANCO")
	String id_banco;
	
	@Column(name = "DESC_BANCOS")
	String desc_bancos;
	
	@Column(name = "ID_RECEPTOR")
	String id_receptor;
	
	@Column(name = "USUARIO")
	String usuario;
	
	@Column(name = "FEC_MODIFICO")
	Date   fec_modifico;
	
	public String getId_banco() {
		return id_banco;
	}
	public void setId_banco(String id_banco) {
		this.id_banco = id_banco;
	}
	public String getDesc_bancos() {
		return desc_bancos;
	}
	public void setDesc_bancos(String desc_bancos) {
		this.desc_bancos = desc_bancos;
	}
	public String getId_receptor() {
		return id_receptor;
	}
	public void setId_receptor(String id_receptor) {
		this.id_receptor = id_receptor;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Date getFec_modifico() {
		return fec_modifico;
	}
	public void setFec_modifico(Date fec_modifico) {
		this.fec_modifico = fec_modifico;
	}
}
