package com.danielcruzmx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "td_plaza")
public class Plaza implements Serializable {
	private static final long serialVersionUID = 1L;

		@Column(name="PLAZA_RFC")
	    private String rfcEmpleado;
	    
	    @Column(name="ID_PLAZA")
	    @Id
	    private Integer idPlaza;
	    
	    @Column(name="ID_UNIDAD_NOM")
	    private String idUnidadNomb;
	    
	    @Column(name="ID_PUESTO_NOM")
	    private String idPuestoNomb;
	    
		@Column(name="ID_GRUPO_PAGO")
	    private String idGrupoPago;  
	    
	    @Column(name="PLAZA_IMP_HONO")
	    private double impHonorario;
	    
	    @Column(name="PLAZA_QNA_CAPTURA")
	    private int quincenaMovimiento;
	    
	    @Column(name="ID_MOVTO_PERSONA")
	    private int movimiento;
	    
	    @Column(name="PLAZA_ULTIMO_MOVTO")
	    @Temporal(javax.persistence.TemporalType.DATE)
	    private java.util.Date fecUltimoMovto;
	    
	    public Plaza(){
	    }

		public String getRfcEmpleado() {
			return rfcEmpleado;
		}

		public void setRfcEmpleado(String rfcEmpleado) {
			this.rfcEmpleado = rfcEmpleado;
		}
		
	    public String getIdPuestoNomb() {
			return idPuestoNomb;
		}

		public void setIdPuestoNomb(String idPuestoNomb) {
			this.idPuestoNomb = idPuestoNomb;
		}

		public Integer getIdPlaza() {
			return idPlaza;
		}

		public void setIdPlaza(Integer idPlaza) {
			this.idPlaza = idPlaza;
		}

		public String getIdUnidadNomb() {
			return idUnidadNomb;
		}

		public void setIdUnidadNomb(String idUnidadNomb) {
			this.idUnidadNomb = idUnidadNomb;
		}

		public String getIdGrupoPago() {
			return idGrupoPago;
		}

		public void setIdGrupoPago(String idGrupoPago) {
			this.idGrupoPago = idGrupoPago;
		}

		public double getImpHonorario() {
			return impHonorario;
		}

		public void setImpHonorario(double impHonorario) {
			this.impHonorario = impHonorario;
		}

		public int getQuincenaMovimiento() {
			return quincenaMovimiento;
		}

		public void setQuincenaMovimiento(int quincenaMovimiento) {
			this.quincenaMovimiento = quincenaMovimiento;
		}

		public int getMovimiento() {
			return movimiento;
		}

		public void setMovimiento(int movimiento) {
			this.movimiento = movimiento;
		}

		public java.util.Date getFecUltimoMovto() {
			return fecUltimoMovto;
		}

		public void setFecUltimoMovto(java.util.Date fecUltimoMovto) {
			this.fecUltimoMovto = fecUltimoMovto;
		}

	
}
