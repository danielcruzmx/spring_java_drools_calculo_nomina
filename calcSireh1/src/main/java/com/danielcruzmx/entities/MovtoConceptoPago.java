package com.danielcruzmx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tn_movto_concepto_pago")
public class MovtoConceptoPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="MCP_RFC")
    private java.lang.String rfc;
	
	@Column(name="ID_MOVTO_CONCEPTO_PAGO")    
	@Id
    private java.lang.Integer idMovtoConceptoPago;
	
	@Column(name="ID_TIPO_CPTO")
    private java.lang.String idTipoCpto;
	
	@Column(name="ID_CONCEPTO")
    private java.lang.String idConcepto;
	
	@Column(name="mcp_MONTO")
    private java.lang.Double monto;
	
	@Column(name="mcp_PORCENTAJE")
    private java.lang.Float porcentaje;
	
	@Column(name="mcp_FEC_INCIDENCIA")
	@Temporal(javax.persistence.TemporalType.DATE)
    private java.util.Date fecIncidencia;
	
	@Column(name="mcp_QNA_FIN")
	private java.math.BigDecimal qnaFin;
    
    @Column(name="mcp_QNA_INI")
    private java.math.BigDecimal qnaIni;
    
    @Column(name="mcp_QNA_APLICADAS")
    private java.math.BigDecimal qnaAplicadas;
    
    @Column(name="mcp_QNA_A_DESCONTAR")
    private java.math.BigDecimal qnaADescontar;
    
    @Column(name="mcp_MONTO_ADEUDO")
    private java.lang.Double montoAdeudo;
    
    @Column(name="mcp_QNAS_ADEUDO")
    private java.math.BigDecimal qnasAdeudo;
    
    @Column(name="ID_SIT_CAPTURA")
    private String idSitCaptura;
    

    public String getIdSitCaptura() {
		return idSitCaptura;
	}


	public void setIdSitCaptura(String idSitCaptura) {
		this.idSitCaptura = idSitCaptura;
	}


	public MovtoConceptoPago(){
	}


	public java.lang.String getRfc() {
		return rfc;
	}


	public void setRfc(java.lang.String rfc) {
		this.rfc = rfc;
	}


	public java.lang.Integer getIdMovtoConceptoPago() {
		return idMovtoConceptoPago;
	}


	public void setIdMovtoConceptoPago(java.lang.Integer idMovtoConceptoPago) {
		this.idMovtoConceptoPago = idMovtoConceptoPago;
	}


	public java.lang.String getIdTipoCpto() {
		return idTipoCpto;
	}


	public void setIdTipoCpto(java.lang.String idTipoCpto) {
		this.idTipoCpto = idTipoCpto;
	}


	public java.lang.String getIdConcepto() {
		return idConcepto;
	}


	public void setIdConcepto(java.lang.String idConcepto) {
		this.idConcepto = idConcepto;
	}


	public java.lang.Double getMonto() {
		return monto;
	}


	public void setMonto(java.lang.Double monto) {
		this.monto = monto;
	}


	public java.lang.Float getPorcentaje() {
		return porcentaje;
	}


	public void setPorcentaje(java.lang.Float porcentaje) {
		this.porcentaje = porcentaje;
	}


	public java.util.Date getFecIncidencia() {
		return fecIncidencia;
	}


	public void setFecIncidencia(java.util.Date fecIncidencia) {
		this.fecIncidencia = fecIncidencia;
	}


	public java.math.BigDecimal getQnaFin() {
		return qnaFin;
	}


	public void setQnaFin(java.math.BigDecimal qnaFin) {
		this.qnaFin = qnaFin;
	}


	public java.math.BigDecimal getQnaIni() {
		return qnaIni;
	}


	public void setQnaIni(java.math.BigDecimal qnaIni) {
		this.qnaIni = qnaIni;
	}


	public java.math.BigDecimal getQnaAplicadas() {
		return qnaAplicadas;
	}


	public void setQnaAplicadas(java.math.BigDecimal qnaAplicadas) {
		this.qnaAplicadas = qnaAplicadas;
	}


	public java.math.BigDecimal getQnaADescontar() {
		return qnaADescontar;
	}


	public void setQnaADescontar(java.math.BigDecimal qnaADescontar) {
		this.qnaADescontar = qnaADescontar;
	}


	public java.lang.Double getMontoAdeudo() {
		return montoAdeudo;
	}


	public void setMontoAdeudo(java.lang.Double montoAdeudo) {
		this.montoAdeudo = montoAdeudo;
	}


	public java.math.BigDecimal getQnasAdeudo() {
		return qnasAdeudo;
	}


	public void setQnasAdeudo(java.math.BigDecimal qnasAdeudo) {
		this.qnasAdeudo = qnasAdeudo;
	}
	

}
