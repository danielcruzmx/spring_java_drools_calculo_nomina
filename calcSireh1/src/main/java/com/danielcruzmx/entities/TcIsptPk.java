package com.danielcruzmx.entities;

import java.io.Serializable;
import java.util.Date;

public class TcIsptPk implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer consecutivo;
    private String cveTipoTabla;
    private Date fecFin;
    
	public Integer getConsecutivo() {
		return consecutivo;
	}
	
	public boolean equals(Object other) {
	    if (this == other)
	        return true;
	    if (!(other instanceof TcIsptPk))
	        return false;
	    TcIsptPk castOther = (TcIsptPk) other;
	    return consecutivo.equals(castOther.consecutivo) && cveTipoTabla.equals(castOther.cveTipoTabla);
	}

	public int hashCode() {
	    final int prime = 31;
	    int hash = 17;
	    hash = hash * prime + this.consecutivo.hashCode();
	    hash = hash * prime + this.cveTipoTabla.hashCode();
	    hash = hash * prime + this.fecFin.hashCode();
	    return hash;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}
	public String getCveTipoTabla() {
		return cveTipoTabla;
	}
	public void setCveTipoTabla(String cveTipoTabla) {
		this.cveTipoTabla = cveTipoTabla;
	}
	public Date getFecFin() {
		return fecFin;
	}
	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}
}
