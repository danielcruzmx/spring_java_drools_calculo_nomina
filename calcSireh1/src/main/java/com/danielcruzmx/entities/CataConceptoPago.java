package com.danielcruzmx.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "tc_concepto_pago")
@IdClass(CataConceptoPagoPk.class)
public class CataConceptoPago {

	@Column(name="ID_TIPO_CPTO")
	@Id
    private java.lang.String idTipoCpto;
	
	@Column(name="ID_CONCEPTO")
	@Id
    private java.lang.String idConcepto;
	
	@Column(name="CP_PRIORIDAD")
    private java.lang.String prioridadCpto;
	
	@Column(name="DESC_CON_PAG")
    private java.lang.String descripcionCpto;

	public java.lang.String getIdTipoCpto() {
		return idTipoCpto;
	}

	public void setIdTipoCpto(java.lang.String idTipoCpto) {
		this.idTipoCpto = idTipoCpto;
	}

	public java.lang.String getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(java.lang.String idConcepto) {
		this.idConcepto = idConcepto;
	}

	public java.lang.String getPrioridadCpto() {
		return prioridadCpto;
	}

	public void setPrioridadCpto(java.lang.String prioridadCpto) {
		this.prioridadCpto = prioridadCpto;
	}

	public java.lang.String getDescripcionCpto() {
		return descripcionCpto;
	}

	public void setDescripcionCpto(java.lang.String descripcionCpto) {
		this.descripcionCpto = descripcionCpto;
	}
	
}
