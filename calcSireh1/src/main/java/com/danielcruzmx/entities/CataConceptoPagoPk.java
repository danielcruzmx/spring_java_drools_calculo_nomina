package com.danielcruzmx.entities;

import java.io.Serializable;

public class CataConceptoPagoPk implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String idTipoCpto;
    private String idConcepto;
    
	public boolean equals(Object other) {
	    if (this == other)
	        return true;
	    if (!(other instanceof TcIsptPk))
	        return false;
	    CataConceptoPagoPk castOther = (CataConceptoPagoPk) other;
	    return idTipoCpto.equals(castOther.idTipoCpto) && idConcepto.equals(castOther.idConcepto);
	}

	public int hashCode() {
	    final int prime = 31;
	    int hash = 17;
	    hash = hash * prime + this.idTipoCpto.hashCode();
	    hash = hash * prime + this.idConcepto.hashCode();
	    return hash;
	}

	public String getIdTipoCpto() {
		return idTipoCpto;
	}

	public void setIdTipoCpto(String idTipoCpto) {
		this.idTipoCpto = idTipoCpto;
	}

	public String getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(String idConcepto) {
		this.idConcepto = idConcepto;
	}




}
