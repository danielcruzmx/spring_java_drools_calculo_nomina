package com.danielcruzmx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "tn_reg_control")
public class Quincena implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_REG_CONTROL")    
	@Id
	private java.lang.Long idQna;
	
	@Column(name="RC_QNA_CAPTURA")
	protected int quincena;
	
	@Column(name="RC_INI")
	@Temporal(javax.persistence.TemporalType.DATE)
	protected java.util.Date fechaInicial;
	
	@Column(name="RC_FIN")
	@Temporal(javax.persistence.TemporalType.DATE)
	protected java.util.Date fechaFinal;
	
	@Column(name="RC_TIPO_CAMBIO")
	private Double tipoCambio;
	
	@Column(name="RC_VIGENTE")
	private String vigente;
	
	public Quincena(){
	}
	
	public String getVigente() {
		return vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}
	
	public int getQuincena() {
		return quincena;
	}

	public void setQuincena(int quincena) {
		this.quincena = quincena;
	}

	public java.util.Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(java.util.Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public java.util.Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(java.util.Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public java.lang.Long getIdQna() {
		return idQna;
	}

	public void setIdQna(java.lang.Long idQna) {
		this.idQna = idQna;
	}

}
