package com.danielcruzmx.entities;

public class ConceptoPagado {
	
	// Atributos base
 	
	private Integer numero;
	private String  tipo;
    private String  cpto;
    private Double  monto;
    private String  descripcion;
    
    public ConceptoPagado() {
    }
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCpto() {
		return cpto;
	}
	public void setCpto(String cpto) {
		this.cpto = cpto;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	@Override
    public String toString() {
        return "Concepto [" + this.tipo + " - " + this.cpto + " -> " + this.monto.toString() + "]";
    }
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
