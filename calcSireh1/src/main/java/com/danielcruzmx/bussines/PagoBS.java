package com.danielcruzmx.bussines;

import java.util.List;

import com.danielcruzmx.entities.CataConceptoPago;
import com.danielcruzmx.entities.ConceptoPagado;
import com.danielcruzmx.bussines.MovtoConceptoPagoBS;
import com.danielcruzmx.entities.Pago;
import com.danielcruzmx.entities.Pension;

public class PagoBS extends Pago{
	private static final long serialVersionUID = 1L;
	
	private double  acumuladoSdoBase;
	private double  acumuladoGravable;
	private double  liquido;
	private double  percepciones;
	private double  deducciones;
	private boolean pension;

	private int     numQuincena;
	
	// Conceptos de salida OUT
	private List<ConceptoPagado> conceptosPagados;
	// Conceptos de entrada IN
	private List<MovtoConceptoPagoBS> conceptos;
	
	private List<Pension> pensiones;

	public PagoBS(){
        this.acumuladoSdoBase  = 0.0;
        this.acumuladoGravable = 0.0;
        this.numQuincena       = 0;
        this.percepciones      = 0.0;
        this.deducciones       = 0.0;
        this.liquido = 0.0;
        this.pension = false;
    }
	
	// Va acumulando el sueldo base para el ISSSTE
	public void setAcumuladoSdoBase(double acumuladoSdoBase) {
		this.acumuladoSdoBase = this.acumuladoSdoBase + acumuladoSdoBase;
	}
	
	// Compara acumulado de sueldo base con el tope
	// regresa el menor de los 2
	public Double sueldoBase(Double tope){
        if(this.acumuladoSdoBase > tope) return tope;
        return this.acumuladoSdoBase;
    }

	// Agrega concepto de pago a conceptos de SALIDA	
	public void setAgregaConcepto(Integer prioridad, String tipo, String cpto, Double monto){
        ConceptoPagado c = new ConceptoPagado();
        prioridad = this.idConcepto(tipo, cpto);
        c.setNumero(prioridad);
        c.setTipo(tipo);
        c.setCpto(cpto);
        c.setMonto(monto);
        this.conceptosPagados.add(c);
    }
	
	// Calcula pensiones y agrega resultado a conceptos de SALIDA
	public void calculaPensiones(String tipo, String clave){
    	Double montoPension = 0.0;
    	for(Pension p: this.getPensiones()){
    		montoPension = this.calculaMontoPensiones(this.getConceptosPagados(), p);
    		this.setAgregaConcepto(p.getNumPension(), tipo, clave, montoPension);
    	}
    }
	
	// Detalle calculo de pensiones 
	public double calculaMontoPensiones(List<ConceptoPagado> cptosPagados, Pension p) {
		Double acumulado = 0.0;
		String cpto  = null;
		String cptosPension = null; 
		// ajustes para conceptos compuestos
		if(p.getCptosPorcentaje().contains("P32")){
			cptosPension= p.getCptosPorcentaje() + "PPV" + "P5E" + "P4E" + "P3G" ;
		} else {
			cptosPension= p.getCptosPorcentaje();
		}
		if(p.getMontoPension() > 0.0 && p.getPorcentajePension() == 0) return p.getMontoPension();
		for(ConceptoPagado c: cptosPagados){
			cpto = c.getTipo().trim() + c.getCpto().trim();
			if(cptosPension.contains(cpto)){
				if(c.getTipo().trim().equals("P")){
					acumulado = acumulado + c.getMonto();
				}
				if(c.getTipo().trim().equals("D")){
					acumulado = acumulado - c.getMonto();
				}
			}
		}
		return acumulado * p.getPorcentajePension() / 100.00 + (p.getMontoAdeudo() / p.getQnaDescAdeudo());
	}

	// Si existe concepto en los conceptos de ENTRADA regresa ID
	public Integer idConcepto(String tipo, String cpto){
	        Integer ret = -1;
	        String tipoCpto = tipo.trim() + cpto.trim();
	        for(MovtoConceptoPagoBS c :  conceptos){
	                String codigo = c.getIdTipoCpto().trim() + c.getIdConcepto().trim();
	                if(codigo.equals(tipoCpto)){
	                        ret = c.getIdMovtoConceptoPago();
	                        break;
	                }
	        }
	        return ret;
	}
	 
	// Si existe concepto en los conceptos de ENTRADA regresa TRUE
	public boolean tieneConcepto(String tipo, String cpto){
	        boolean ret = false;
	        String tipoCpto = tipo.trim() + cpto.trim();
	        for(MovtoConceptoPagoBS c :  conceptos){
	                String codigo = c.getIdTipoCpto().trim() + c.getIdConcepto().trim(); 
	                if(codigo.equals(tipoCpto)){
	                        ret = true;
	                        break;
	                }
	        }
	        return ret;
	} 
	
	// Calcula liquido sobre conceptos de SALIDA
	public void calculaLiquido(){
        Double percepciones = 0.0;
        Double deducciones  = 0.0; 
        for(ConceptoPagado cp : conceptosPagados){
              if(cp.getTipo().contains("P")) percepciones = percepciones + cp.getMonto();
                    if(cp.getTipo().contains("D")) deducciones  = deducciones  + cp.getMonto();
        }
        this.setDeducciones(deducciones);
        this.setPercepciones(percepciones);
        this.setLiquido(percepciones - deducciones);
    }
	
	// Da descripcion a los conceptos de SALIDA
	public void fillDescCptosSalida(List<CataConceptoPago> catalogoCptos) {
		for(ConceptoPagado cp : conceptosPagados){
			String cl = cp.getTipo().trim() + cp.getCpto().trim();
			for(CataConceptoPago cat : catalogoCptos){
				String cc = cat.getIdTipoCpto().trim() + cat.getIdConcepto().trim();
				if(cl.equals(cc)) {
					cp.setDescripcion(cat.getDescripcionCpto().toUpperCase());
				}
			}
		}
	}

	// Da descripcion a los conceptos de ENTRADA
	public void fillDescCptosEntrada(List<CataConceptoPago> catalogoCptos) {
		for(MovtoConceptoPagoBS cp : conceptos){
			String cl = cp.getIdTipoCpto().trim() + cp.getIdConcepto().trim();
			for(CataConceptoPago cat : catalogoCptos){
				String cc = cat.getIdTipoCpto().trim() + cat.getIdConcepto().trim();
				if(cl.equals(cc)) {
					cp.setDescripcion(cat.getDescripcionCpto().toUpperCase());
				}
			}
		}
	}

	// Recupera el monto de un concepto de ENTRADA
	public Double montoConcepto(String tipo, String cpto){
        Double ret = 0.0;
        String tipoCpto = tipo.trim() + cpto.trim();
        for(MovtoConceptoPagoBS c :  conceptos){
                String codigo = c.getIdTipoCpto().trim() + c.getIdConcepto().trim();
                if(codigo.equals(tipoCpto)){
                        ret = c.getMonto();
                        break;
                }
        }
        return ret;
    }
	
	// Recupera el porcentaje de un concepto de ENTRADA
	public Float porcentajeConcepto(String tipo, String cpto){
        Float ret = 0.00f;
        String tipoCpto = tipo.trim() + cpto.trim();
        for(MovtoConceptoPagoBS c :  conceptos){
            String codigo = c.getIdTipoCpto().trim() + c.getIdConcepto().trim(); 
                if(codigo.equals(tipoCpto)){
                        ret = c.getPorcentaje();
                        break;
                }
        }
        return ret;
   }
	
    // Va acumulando la base Gravable
    public void setAcumuladoGravable(double acumuladoGravable) {
	       this.acumuladoGravable = this.acumuladoGravable + acumuladoGravable;
	}
	
    
    public double getPercepciones() {
		return percepciones;
	}

	public void setPercepciones(double percepciones) {
		this.percepciones = percepciones;
	}

	public double getDeducciones() {
		return deducciones;
	}

	public void setDeducciones(double deducciones) {
		this.deducciones = deducciones;
	}
    
    public int getNumQuincena() {
		return numQuincena;
	}

	public void setNumQuincena(int numQuincena) {
		this.numQuincena = numQuincena;
	}
   
	public double getAcumuladoSdoBase() {
		return acumuladoSdoBase;
	}

	public double getAcumuladoGravable() {
		return acumuladoGravable;
	}
	
	public List<MovtoConceptoPagoBS> getConceptos() {
		return conceptos;
	}

	public void setConceptos(List<MovtoConceptoPagoBS> conceptos) {
		this.conceptos = conceptos;
	}
	
	public List<ConceptoPagado> getConceptosPagados() {
		return conceptosPagados;
	}

	public void setConceptosPagados(List<ConceptoPagado> conceptosPagados) {
		this.conceptosPagados = conceptosPagados;
	}

	public boolean isPension() {
		return pension;
	}

	public void setPension(boolean pension) {
		this.pension = pension;
	}

	public double getLiquido() {
		return liquido;
	}

	public void setLiquido(double liquido) {
		this.liquido = liquido;
	}
	
	public List<Pension> getPensiones() {
		return pensiones;
	}

	public void setPensiones(List<Pension> pensiones) {
		this.pensiones = pensiones;
	}
	
}
