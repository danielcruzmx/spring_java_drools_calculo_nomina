package com.danielcruzmx.bussines;

import com.danielcruzmx.entities.MovtoConceptoPago;

public class MovtoConceptoPagoBS extends MovtoConceptoPago{
	private static final long serialVersionUID = 1L;
	
	private String descripcion;
	
	public MovtoConceptoPagoBS(){
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
