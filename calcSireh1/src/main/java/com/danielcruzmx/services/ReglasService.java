package com.danielcruzmx.services;

import org.springframework.stereotype.Service;

import com.danielcruzmx.repositories.FileAccess;

import java.io.Reader;
import org.drools.RuleBase;
import org.drools.RuleBaseFactory;
import org.drools.WorkingMemory;
import org.drools.compiler.PackageBuilder;

@Service
public class ReglasService {
	
	private static String SENTENCIAS = "Sentencias.dsl";
	private static String REGLAS     = "Reglas.dslr";
	
	private Reader dsl;
	private Reader dslr;
	private RuleBase reglas;
	
	public ReglasService(){
		FileAccess archivo = new FileAccess();
		System.out.println("\n Lee archivos de reglas ");
		try {
			// Carga archivos y compila
			this.setDsl(archivo.lee(SENTENCIAS));
			this.setDslr(archivo.lee(REGLAS));
			this.setReglas(this.leer());
		} catch	(Throwable t) {
	    	 System.out.println("\n ERROR AL LEER Y COMPILAR LAS REGLAS ");
	         t.printStackTrace();
		}
	}
	
	public boolean evalua(Object obj){
		boolean ret = false;
		try {
	            RuleBase ruleBase = this.getReglas();
	            WorkingMemory workingMemory = ruleBase.newStatefulSession();
                workingMemory.insert(obj);
		        workingMemory.fireAllRules();
		        ret = true ;
	     } catch (Throwable t) {
	    	 System.out.println("\n ERROR AL EVALUAR OBJETO ");
	         t.printStackTrace();
	     }
		return ret; 
	}
	
	private RuleBase leer() throws Exception {
        PackageBuilder builder = new PackageBuilder();
        builder.addPackageFromDrl(this.getDslr(),this.getDsl());

        if (builder.hasErrors()) {
            System.out.println(builder.getErrors().toString());
            throw new RuntimeException(
                "No se pudo compilar el archivo de reglas.");
        }
 
        org.drools.rule.Package pkg = builder.getPackage();
        RuleBase ruleBase = RuleBaseFactory.newRuleBase();
        ruleBase.addPackage(pkg);
        return ruleBase;
    }
	

	public RuleBase getReglas() {
		return reglas;
	}

	public void setReglas(RuleBase reglas) {
		this.reglas = reglas;
	}

	public Reader getDslr() {
		return dslr;
	}

	public void setDslr(Reader dslr) {
		this.dslr = dslr;
	}

	public Reader getDsl() {
		return dsl;
	}

	public void setDsl(Reader dsl) {
		this.dsl = dsl;
	}    

}
