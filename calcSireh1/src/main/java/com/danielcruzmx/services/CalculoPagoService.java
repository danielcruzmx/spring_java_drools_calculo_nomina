package com.danielcruzmx.services;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danielcruzmx.repositories.CataConceptoPagoRepo;
import com.danielcruzmx.repositories.MovtoConceptoPagoRepo;
import com.danielcruzmx.repositories.PensionRepo;
import com.danielcruzmx.bussines.MovtoConceptoPagoBS;
import com.danielcruzmx.bussines.PagoBS;
import com.danielcruzmx.entities.ConceptoPagado;
import com.danielcruzmx.entities.MovtoConceptoPago;
import com.danielcruzmx.entities.Pago;
import com.danielcruzmx.entities.Pension;


@Service
public class CalculoPagoService {

	private PagoBS pagoBS;
	private MovtoConceptoPagoBS cptoBS;
	
	@Autowired
	private MovtoConceptoPagoRepo repositoryConceptoPago;
	
	@Autowired
	private PensionRepo repositoryPension;
	
	//@Autowired
	//CataConceptoPagoRepo catalogoConceptos;

	@Autowired
	private ReglasService reglas;
	
	@Autowired
	private QuincenaProcesoService qnaProceso;
	
	@Autowired
	private DescConceptosService descCptos;
	
	DozerBeanMapper mapper = new DozerBeanMapper();
	
	public CalculoPagoService(){
	}
	
	public boolean evalua(Pago p) {
		List<MovtoConceptoPago>   cptosIn   = new ArrayList<MovtoConceptoPago>();
		List<MovtoConceptoPagoBS> cptosInBS = new ArrayList<MovtoConceptoPagoBS>();
		List<ConceptoPagado>      cptosOut  = new ArrayList<ConceptoPagado>();
		List<Pension>             pensiones = new ArrayList<Pension>();
		
		boolean ret = false;
		
		//System.out.println(p.getRfcEmpleado());
		cptosIn   = repositoryConceptoPago.findByRfc(p.getRfcEmpleado());
		for(MovtoConceptoPago c : cptosIn) {
			cptoBS = mapper.map(c, MovtoConceptoPagoBS.class);
			cptosInBS.add(cptoBS);
		}
		
		pensiones = repositoryPension.findByRfcPension(p.getRfcEmpleado());
		//pensiones.forEach(x -> System.out.println(x.getBeneficiariaPension()));
		
		// Completa objeto pago con mas informacion para evaluacion
		// Mapea objeto con datos de la Base de Datos
		pagoBS = mapper.map(p, PagoBS.class);
		// Establece conceptos de pago (ENTRADA)
		pagoBS.setConceptos(cptosInBS);
		// Inicializa conceptos pagados (SALIDA)
		pagoBS.setConceptosPagados(cptosOut);
		// Establece pensiones
		pagoBS.setPensiones(pensiones);
		if(! pensiones.isEmpty()) {
			System.out.println("Tiene Pension");
			pagoBS.setPension(true);
		}
		// Agrega quincena de proceso al objeto
		pagoBS.setNumQuincena(qnaProceso.getNumero());
		
		// evalua objeto pago, calcula segun reglas
		ret = reglas.evalua(pagoBS);
		
		// complementa el objet pago
		pagoBS.calculaLiquido();
		pagoBS.fillDescCptosSalida(descCptos.getCatalogoCptos());
		pagoBS.fillDescCptosEntrada(descCptos.getCatalogoCptos());
		return ret;
	}

	public PagoBS getPago() {
		return pagoBS;
	}

	public void setPago(PagoBS pagoBS) {
		this.pagoBS = pagoBS;
	}

}
