package com.danielcruzmx.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.danielcruzmx.entities.CataConceptoPago;

@Service
public class DescConceptosService {
	
	private static List<CataConceptoPago> catalogoCptos = null;
	
    public DescConceptosService() {
    }
    
	public String getDescCpto(String idTipo, String idCpto) {
		String ret = "NO HAY DESCRIPCION";
		String tipoCpto = idTipo.trim() + idCpto.trim();
		for (CataConceptoPago c: catalogoCptos) {
			String codigo = c.getIdTipoCpto().trim() + c.getIdConcepto().trim(); 
            if(codigo.equals(tipoCpto)){
                    ret = c.getDescripcionCpto().toUpperCase();
                    break;
            }
		}
		return ret;
	}
	
	public List<CataConceptoPago> getCatalogoCptos() {
		return catalogoCptos;
	}

	public void setCatalogoCptos(List<CataConceptoPago> catalogoCptos) {
		DescConceptosService.catalogoCptos = catalogoCptos;
	}

}
