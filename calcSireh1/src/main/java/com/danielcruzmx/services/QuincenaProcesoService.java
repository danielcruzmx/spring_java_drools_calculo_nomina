package com.danielcruzmx.services;

import java.text.DecimalFormat;
import java.util.GregorianCalendar;

import org.springframework.stereotype.Service;

import com.danielcruzmx.entities.Quincena;

@Service
public class QuincenaProcesoService {
	
	private int quincena;
	private int diasQuincena;
	private int numero;
	private int anio;
	private int mes;

	private java.util.Date fechaFinal;
	private java.util.Date fechaInicial;
	
	public QuincenaProcesoService() {
		
	}
	
	public void init(Quincena q) {
		this.quincena = q.getQuincena();
		this.anio = Integer.parseInt(String.valueOf(this.quincena).substring(0, 4));
		this.numero = Integer.parseInt(String.valueOf(this.quincena).substring(4, 6));
		this.mes = (int)Math.floor((this.numero + 1)/2);
		this.fechaInicial = qnaAFechaIni();
		this.fechaFinal = qnaAFechaFin();
		long diff = this.fechaFinal.getTime() - this.fechaInicial.getTime();
		this.diasQuincena = (int) (diff / (24 * 60 * 60 * 1000) + 1);
	}

	private java.util.Date qnaAFechaIni(){
		int dia;
		java.util.Date fecha;
		if((this.numero % 2) == 0)dia = 16;
		else dia = 1;	
		GregorianCalendar diag = new GregorianCalendar(this.anio, this.mes -1, dia);
		fecha = diag.getTime();
		return fecha;
	}
	
	private java.util.Date qnaAFechaFin(){
		GregorianCalendar cal = 
	              (GregorianCalendar) GregorianCalendar.getInstance();
		java.util.Date fecha;
		if((this.numero % 2) == 0) {
			int max = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
			GregorianCalendar diag = new GregorianCalendar(this.anio, this.mes -1, max);
			fecha = diag.getTime();
		} else {
			GregorianCalendar diag = new GregorianCalendar(this.anio, this.mes -1, 15);
			fecha = diag.getTime();	
		}
		return fecha;
	}
	
	private String integerFormat(int i, String formato) { 
		DecimalFormat df = new DecimalFormat(formato); 
		String s = df.format(i); 
		return s; 
	}
	
	public int getDiasQuincena() {
		return diasQuincena;
	}

	public void setDiasQuincena(int diasQuincena) {
		this.diasQuincena = diasQuincena;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}
	
	public java.util.Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(java.util.Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public java.util.Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(java.util.Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
	public int getQuincena() {
		return quincena;
	}

	public void setQuincena(int quincena) {
		this.quincena = quincena;
	}


	public String toString() {
		return "Quincena [Numero=" + integerFormat(numero,"00").toString() + ", Anio=" + integerFormat(anio,"00").toString() + 
				", Mes=" + integerFormat(mes,"00").toString() + ", Quincena=" + integerFormat(quincena,"000000").toString() + "]";
	}
	
}
