package com.danielcruzmx.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.danielcruzmx.entities.TcIspt;

@Service
public class CalculoImpuestoService {

	private static List<TcIspt> tablaIspt = null;
    private static List<TcIspt> tablaBrutos = null;

    public CalculoImpuestoService() {
    }

    public void setTablaIspt(List<TcIspt> tabla) {
        CalculoImpuestoService.tablaIspt = tabla;
    }

    public void setTablaBrutos(List<TcIspt> tabla) {
        CalculoImpuestoService.tablaBrutos = tabla;
    }

    public static Double isr(Double baseGravable) {
        Double ispt = 0.0;
        for (TcIspt next: tablaIspt) {
            if (baseGravable >= next.getLimiteInferior1() && baseGravable <= next.getLimiteSuperior()) {
                ispt = (baseGravable - next.getLimiteInferior1()) * (next.getExcedente() / 100.0) +  next.getCuotaFija();
                break;
            }
        }
        return ispt;
    }

    public static Double bruto(Double base) {
        Double ispt = 0.0;
        for (TcIspt next: tablaBrutos) {
            if (base >= next.getLimiteInferior1() &&  base <= next.getLimiteSuperior()) {
                ispt = (base - next.getCuotaFija()) / next.getSueldoBruto2();
                break;
            }
        }
        return ispt;
    }
    
    public static Double redondear(Double base) {
        Double valor = 0.0;
        valor = Math.rint(base*100)/100;
        return valor;
    }
}
