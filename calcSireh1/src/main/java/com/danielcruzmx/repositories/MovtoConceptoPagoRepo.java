package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.MovtoConceptoPago;

public interface MovtoConceptoPagoRepo extends JpaRepository <MovtoConceptoPago, Integer> {
	
	@Query("select c from MovtoConceptoPago c where c.idSitCaptura = 'V' and c.rfc = :rfc")
	List<MovtoConceptoPago> findByRfc(@Param("rfc") String rfc);
	
}
