[keyword][]PERCEPCION            = "P"
[keyword][]DEDUCCION             = "D"
[keyword][]CPTO_CONTROL          = "C"
[keyword][]SUELDO_QNA            = (pago.getTabSueldo()/2.0)
[keyword][]COMPENSACION_QNA      = (pago.getTabCompensacion()/2.0)
[keyword][]BASE_GRAVABLE         = (pago.getBaseGravable)
[keyword][]AYUDA_DESP_MANDO_QNA  = (1215.0 / 2.0) 
[keyword][]AYUDA_DESP_OPERA_QNA  = (1215.0 / 2.0)
[keyword][]QUINQUENIO_A1_QNA     = (160.0  / 2.0)
[keyword][]QUINQUENIO_A2_QNA     = (185.0  / 2.0)
[keyword][]QUINQUENIO_A3_QNA     = (235.0  / 2.0)
[keyword][]QUINQUENIO_A4_QNA     = (260.0  / 2.0)
[keyword][]QUINQUENIO_A5_QNA     = (285.0  / 2.0)
[keyword][]PREVISION_SOCIAL_QNA  = (845.0  / 2.0) 
[keyword][]AYUDA_SERVICIOS_QNA   = (805.0  / 2.0)
[keyword][]AYUDA_TRANSPORTE_QNA  = (960.0  / 2.0)
[keyword][]COMP_X_DESA_Y_CAP_QNA = (2000.0 / 2.0)
[keyword][]CUOTA_ISSSTE_0A       = 0.0275
[keyword][]CUOTA_ISSSTE_0B       = 0.00625
[keyword][]CUOTA_ISSSTE_0C       = 0.005
[keyword][]CUOTA_ISSSTE_0D       = 0.06125
[keyword][]CUOTA_ISSSTE_0E       = 0.00625
[keyword][]CUOTA_SINDICAL        = 0.015
[keyword][]FONDO_RETIRO_SINDICAL = 0.01
[keyword][]SEG_COLECTIVO_QNA_PAR = 7.28
[keyword][]SEG_COLECTIVO_QNA_NON = 7.27
[keyword][]MONTO_FONAC_QNA       = 310.76
[keyword][]TOPE_SDO_BASE_QNA     = pago.sueldoBase(14433.00) 
[keyword][]GRAVABLE              = pago.getAcumuladoGravable()
[keyword][]PORCENTAJE_CPTO "{tipo}" "{concepto}"= pago.porcentajeConcepto("{tipo}","{concepto}")
[keyword][]MONTO_CPTO "{tipo}" "{concepto}"     = redondear(pago.montoConcepto("{tipo}","{concepto}"))
[condition][]Test                = eval( 1==1 )
[condition][]Empleado            = pago : PagoBS()
[condition][]-es eventual        = idGrupoPago == 'V'
[condition][]-es estructura      = idGrupoPago == 'E'
[condition][]-es honorario       = idGrupoPago == 'H'
[condition][]-no es honorario    = idGrupoPago != 'H'
[condition][]-es operativo       = idJerarquia == '7'
[condition][]-es de base         = idNombramiento == 'B'
[condition][]-es de confianza    = idNombramiento == 'C'
[condition][]-es mando o enlace  = idJerarquia == '4' || idJerarquia == '5'
[condition][]-es mando superior  = idJerarquia == '1' 
[condition][]-tiene pension      = pension == true
[condition][]-es quincena par    = numQuincena % 2 == 0
[condition][]-es quincena non    = numQuincena % 2 != 0
[condition][]-tiene FONAC ordinario       = tieneConcepto("U","F")
[condition][]-tiene FONAC extraordinario  = tieneConcepto("U","FE")
[condition][]-tiene "{tipo}" "{concepto}" = tieneConcepto("{tipo}","{concepto}")
[consequence][]Asigna "{tipo}" "{concepto}" valor {valor} = pago.setAgregaConcepto(1, "{tipo}", "{concepto}", {valor} );
[consequence][]A gravable {valor}        = pago.setAcumuladoGravable({valor});
[consequence][]A sdo base ISSSTE {valor} = pago.setAcumuladoSdoBase({valor});
[consequence][]Asigna pensiones "{tipo}" "{concepto}" = pago.calculaPensiones("{tipo}","{concepto}");
[consequence][]Imprime                   = System.out.println("Reglas OK");