package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.danielcruzmx.entities.CataConceptoPago;

public interface CataConceptoPagoRepo extends JpaRepository <CataConceptoPago, String>{

	@Query("select c from CataConceptoPago c where c.idTipoCpto in ('P','D','U','C') order by c.idTipoCpto, c.idConcepto ")
	List<CataConceptoPago> findAll();
	
}
