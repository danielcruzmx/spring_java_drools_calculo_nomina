package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielcruzmx.entities.Pago;

public interface PagoRepo extends JpaRepository <Pago, String>{

	List<Pago> findByRfcEmpleado(String rfcEmpleado);
		
}
