package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.Pension;

public interface PensionRepo extends JpaRepository <Pension, Long>{

	@Query("select p from Pension p where p.statusPension = 'V' and p.rfcPension = :rfcPension")
	List<Pension> findByRfcPension(@Param("rfcPension") String rfcPension);
	
}
