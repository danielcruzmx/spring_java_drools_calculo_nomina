package com.danielcruzmx.repositories;

import java.io.InputStreamReader;
import java.io.Reader;

import org.springframework.stereotype.Repository;

@Repository
public class FileAccess {
	
	public FileAccess(){
	}
	
	public Reader lee(String fileName){
		Reader source = new InputStreamReader(
        		this.getClass().getResourceAsStream(fileName));
		return source;
	}

}
