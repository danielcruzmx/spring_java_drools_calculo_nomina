package com.danielcruzmx.repositories;

import java.util.stream.Stream;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.Banco;

public interface BancoRepo extends CrudRepository<Banco, String>{

	@Query("select b from Banco b where b.id_banco like :id_banco")
	Stream<Banco> findById_banco(@Param("id_banco") String id_banco);
	
	
}
