package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.Empleado;

public interface EmpleadoRepo extends JpaRepository<Empleado, String>{

	@Query("select e from Empleado e where e.rfcEmpleado like :rfcEmpleado order by e.rfcEmpleado")
	List<Empleado> findByRfcEmpleado(@Param("rfcEmpleado") String rfcEmpleado, Pageable pageable);
	
	@Query("select e from Empleado e where e.primerApellido like :primerApellido order by e.rfcEmpleado")
	List<Empleado> findByPrimeApellido(@Param("primerApellido") String primerApellido, Pageable pageable);
	
	@Query("select e from Empleado e where e.segundoApellido like :segundoApellido order by e.rfcEmpleado")
	List<Empleado> findBySegundoApellido(@Param("segundoApellido") String segundoApellido, Pageable pageable);
	
	@Query("select e from Empleado e where "
			+ "	e.segundoApellido like :segundoApellido and "
			+ " e.primerApellido  like :primerApellido  and "
			+ " e.nombreEmpleado  like :nombreEmpleado  and "
			+ " e.rfcEmpleado     like :rfcEmpleado     and "
			+ " e.rfcEmpleado = e.rfcUnico and "
			+ " length(e.rfcEmpleado) = 13 order by e.rfcEmpleado" )
	List<Empleado> findAll(@Param("primerApellido")  String primerApellido,
						   @Param("segundoApellido") String segundoApellido,
						   @Param("nombreEmpleado")  String nomreEmpleado,
						   @Param("rfcEmpleado")     String rfcEmpleado, Pageable pageable);
	
	@Query(value = ""
			+ "select * from td_empleado  "
			+ "where nombre_empleado like :nombreEmpleado  "
			+ "and rfc_empleado like      :rfcEmpleado     "
			+ "and primer_apellido like   :primerApellido  "
			+ "and segundo_apellido like  :segundoApellido "
			+ "and length(rfc_empleado) = 13               "
			+ "and rfc_empleado in (select plaza_rfc       "
			+ "                     from td_plaza          "
			+ "                     where id_sit_plaza like 'O%') "
			+ "",
		   nativeQuery = true)
	List<Empleado> findVigentes(@Param("primerApellido")  String primerApellido,
			   @Param("segundoApellido") String segundoApellido,
			   @Param("nombreEmpleado")  String nomreEmpleado,
			   @Param("rfcEmpleado")     String rfcEmpleado, Pageable pageable);
	
}
