package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.TcIspt;
import com.danielcruzmx.entities.TcIsptPk;

public interface TcIsptRepo extends JpaRepository <TcIspt, TcIsptPk>{

	@Query("select i from TcIspt i where i.fecFin = '01/01/2099' and i.cveTipoTabla = :cveTipoTabla")
	List<TcIspt> findByCveTipoTabla(@Param("cveTipoTabla") String cveTipoTabla);
	
}
