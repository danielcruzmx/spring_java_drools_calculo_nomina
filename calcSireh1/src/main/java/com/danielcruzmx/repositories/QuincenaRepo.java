package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.danielcruzmx.entities.Quincena;

public interface QuincenaRepo extends JpaRepository <Quincena, Long>{

	@Query("select q from Quincena q where q.vigente = 'S'")
	List<Quincena> findByVigente();
	
}
