package com.danielcruzmx.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.danielcruzmx.entities.Plaza;

public interface PlazaRepo extends JpaRepository <Plaza, String> {
	
	@Query("select p from Plaza p where p.idPuestoNomb like :idPuestoNomb order by p.rfcEmpleado")
	List<Plaza> findByIdPuestoNomb(@Param("idPuestoNomb") String idPuestoNomb, Pageable pageable);
	
	@Query("select p from Plaza p where p.rfcEmpleado like :rfcEmpleado order by p.rfcEmpleado")
	List<Plaza> findByRfcEmpleado(@Param("rfcEmpleado") String rfcEmpleado, Pageable pageable);
	
	@Query("select p from Plaza p where p.idUnidadNomb = :idUnidadNomb order by p.rfcEmpleado")
	List<Plaza> findByIdUnidadNomb(@Param("idUnidadNomb") String idUnidadNomb, Pageable pageable);
	
}
