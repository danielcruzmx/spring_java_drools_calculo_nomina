package com.danielcruzmx;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import com.danielcruzmx.entities.CataConceptoPago;
import com.danielcruzmx.entities.Quincena;
import com.danielcruzmx.entities.TcIspt;
import com.danielcruzmx.repositories.CataConceptoPagoRepo;
import com.danielcruzmx.repositories.QuincenaRepo;
import com.danielcruzmx.repositories.TcIsptRepo;

import com.danielcruzmx.services.CalculoImpuestoService;
import com.danielcruzmx.services.DescConceptosService;
import com.danielcruzmx.services.QuincenaProcesoService;
import com.danielcruzmx.services.ReglasService;


@SpringBootApplication
public class CalcSireh1Application implements CommandLineRunner{

	/*@Autowired
	PagoDao pagoDao;*/
	
	@Autowired
	TcIsptRepo tablaIsptRepo; 
	
	@Autowired
	QuincenaRepo quincena; 
	
	@Autowired
	CataConceptoPagoRepo catalogoConceptos;
	
	@Autowired
	CalculoImpuestoService calculo;
	
	@Autowired
	ReglasService reglas;
	
	@Autowired
	QuincenaProcesoService qnaProceso;
	
	@Autowired
	DescConceptosService conceptos;
	
	//@Autowired
	//private ConceptoPagoDao repositoryConceptoPago;
		
	public static void main(String[] args) throws Exception {
		SpringApplication.run(CalcSireh1Application.class, args);
	}	
		
	@Transactional(readOnly = true)
	@Override
	public void run(String... strings) throws Exception {
				
		/*DozerBeanMapper mapper = new DozerBeanMapper();
		
		// Trae empleados
		System.out.println("\n Busqueda de Plazas por RFC ");
		List<Pago> lstPagos = pagoDao.findByRfcEmpleado("CUCD%");
		
		for(Pago p: lstPagos){
			PagoDto pago = mapper.map(p, PagoDto.class);
			
			// Inicializa arreglos de conceptos de entrada y salida
			List<ConceptoPago>   cptosIn  = new ArrayList<ConceptoPago>();
			List<ConceptoPagado> cptosOut = new ArrayList<ConceptoPagado>();
			pago.setConceptos(cptosIn);
			pago.setConceptosPagados(cptosOut);
			
			// Calcula conceptos (evalua reglas)
			if(reglas.evalua(pago)) {
			
				// Muestra resultados
				System.out.println(pago.toString());
				for(ConceptoPagado cptos: pago.getConceptosPagados()) {
					System.out.println(cptos.toString());
				}
			}
		}*/
		
		/*System.out.println("\n Sueldo ");
		double sueldo = pago.get(0).getTabSueldo();
		System.out.println(sueldo);*/
		
		System.out.println("\n Tablas de impuestos ");
		List<TcIspt> tablaIspt   = tablaIsptRepo.findByCveTipoTabla("80");
		List<TcIspt> tablaBrutos = tablaIsptRepo.findByCveTipoTabla("REVA");
		calculo.setTablaIspt(tablaIspt);
		calculo.setTablaBrutos(tablaBrutos);
		
		System.out.println("\n Descripcion de conceptos ");
		List<CataConceptoPago> catalogo = catalogoConceptos.findAll(); 
		conceptos.setCatalogoCptos(catalogo);
		
		//catalogo.forEach(x -> System.out.println(x.getIdTipoCpto() + x.getIdConcepto() + "->" + x.getDescripcionCpto()));
		
		// Recupera quincena de proceso
		Quincena q = quincena.findByVigente().get(0);
		qnaProceso.init(q);
		System.out.println(qnaProceso.toString());
		
		
		/*System.out.println("\n Impuesto ");
		System.out.println(calculo.isr(Double.valueOf(sueldo)));*/
				
		/*SimpleDateFormat  sdf = new SimpleDateFormat("dd/mm/yyyy");
		Date d = sdf.parse("01/01/2099");*/
		
		/*List<TcIspt> tablaIspt2 = tablaIsptDao.findByFecFin(d);
		tablaIspt2.forEach(x -> System.out.println(x.getFecFin().toString()));*/
		
		/*List<ConceptoPago> cptos = repositoryConceptoPago.findByRfc("CUCD6308017Y1");
		cptos.forEach(x -> System.out.println(x.getIdConcepto()));*/
		
	}
}