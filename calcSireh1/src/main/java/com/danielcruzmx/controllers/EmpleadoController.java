package com.danielcruzmx.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.danielcruzmx.entities.Empleado;
import com.danielcruzmx.entities.Pago;
import com.danielcruzmx.repositories.EmpleadoRepo;
import com.danielcruzmx.repositories.PagoRepo;
import com.danielcruzmx.services.CalculoPagoService;

@Controller
public class EmpleadoController {

	@Autowired
	private EmpleadoRepo repositoryEmpleado;
	
	@Autowired
	private PagoRepo repositoryPago;
	
	@Autowired
	private CalculoPagoService calculoPago;
	
	@RequestMapping({"/empleados", "/empleados"})
    public String empleadosPage(@ModelAttribute("criterio") Empleado criterio, Model modelA, Model modelB) {
        
		Empleado emp  = new Empleado();
		modelA.addAttribute("empleado", emp);
		
		Pageable pageable = PageRequest.of(0, 10);
		
		if(criterio.getRfcEmpleado()     != null && criterio.getPrimerApellido() != null &&  
		   criterio.getSegundoApellido() != null && criterio.getNombreEmpleado() != null) {
			
				modelB.addAttribute("empleados", repositoryEmpleado.findVigentes(criterio.getPrimerApellido().toUpperCase()  + "%",
																			     criterio.getSegundoApellido().toUpperCase() + "%",
																			     criterio.getNombreEmpleado().toUpperCase()  + "%",
																			     criterio.getRfcEmpleado().toUpperCase()     + "%", pageable));
		}
		return "empleados";
    }
	
	
	@RequestMapping("/conceptos")
	public String conceptosPage(@ModelAttribute("empleado") String rfc, Model model ) {
		//System.out.println("Calculo para :");
		//System.out.println(rfc);
		List<Pago> emp = repositoryPago.findByRfcEmpleado(rfc);
		if(calculoPago.evalua(emp.get(0))) {
			model.addAttribute("pago", calculoPago.getPago());
			return "conceptosempleado";
		}
		model.addAttribute("empleado", rfc);
		return "errorcalculo";
	}
}
