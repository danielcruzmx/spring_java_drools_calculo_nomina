package com.danielcruzmx.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.danielcruzmx.repositories.PlazaRepo;
import com.danielcruzmx.repositories.PagoRepo;
import com.danielcruzmx.entities.Pago;
import com.danielcruzmx.entities.Plaza;
import com.danielcruzmx.services.CalculoPagoService;


//@RestController
@Controller
//@RequestMapping("/calculo/nomina")
public class PagoController {
	
	@Autowired
	private PlazaRepo repositoryPlaza;
	
	@Autowired
	private PagoRepo repositoryPago;
	
	@Autowired
	private CalculoPagoService calculoPago;
	
	// Para RestController
	/*@GetMapping("/empleados/{rfc}")
	public List<Pago> findByRfc(@PathVariable("rfc") String rfc){
		return repository.findByRfcEmpleado(rfc + "%");
	}*/
	
	// RUTAS PARA USO DE TEMPLATES HTML THYMELEAF
	
	@RequestMapping({"/","/listaempleados"})
	public String Inicio(@ModelAttribute("criterio") Plaza criterio, Model modelA, Model modelB) {
		Plaza plaza  = new Plaza();
		modelA.addAttribute("plaza", plaza);
		
		Pageable pageable = PageRequest.of(0, 10);
		
		if(criterio.getRfcEmpleado() != null && criterio.getIdPuestoNomb() != null &&  criterio.getIdUnidadNomb() != null) {
			if(!criterio.getRfcEmpleado().isEmpty()) {
				//System.out.println("Por RFC ");
				//System.out.println(criterio.getRfcEmpleado() + "%");
				modelB.addAttribute("allEmpleados", repositoryPlaza.findByRfcEmpleado(criterio.getRfcEmpleado().toUpperCase() + "%", pageable));
			} else if (!criterio.getIdPuestoNomb().isEmpty()) {
				//System.out.println("Por Puesto");
				//System.out.println(criterio.getIdPuestoNomb() + "%");
				modelB.addAttribute("allEmpleados", repositoryPlaza.findByIdPuestoNomb(criterio.getIdPuestoNomb().toUpperCase() + "%", pageable));
			} else if (!criterio.getIdUnidadNomb().isEmpty()) {
				//System.out.println("Por Unidad");
				//System.out.println(criterio.getIdUnidadNomb());
				modelB.addAttribute("allEmpleados", repositoryPlaza.findByIdUnidadNomb(criterio.getIdUnidadNomb().toUpperCase(), pageable));
			}
		}
		
		//modelB.addAttribute("allEmpleados", repositoryPlaza.findAll(pageable));
		return "listaempleados";
	}
	
	@RequestMapping("/resultado")
	public String resultadoCalculo(@ModelAttribute("empleado") String rfc, Model model ) {
		//System.out.println("Calculo para :");
		//System.out.println(rfc);
		List<Pago> emp = repositoryPago.findByRfcEmpleado(rfc);
		if(calculoPago.evalua(emp.get(0))) {
			model.addAttribute("pago", calculoPago.getPago());
			//System.out.println(calculoPago.getPago().toString());
			return "resultadocalculo";
		}
		model.addAttribute("empleado", rfc);
		return "errorcalculo";
	}

	// RUTAS PARA USO DE JSP
	
	@RequestMapping("/procesarCriterio")
	public String procesarCriterio(@ModelAttribute("criterio") Plaza criterio, Model model ) {
		
		Pageable pageable = PageRequest.of(0, 10);
		
		if(!criterio.getRfcEmpleado().isEmpty()) {
			//System.out.println("Por RFC ");
			//System.out.println(criterio.getRfcEmpleado() + "%");
			model.addAttribute("empleados", repositoryPlaza.findByRfcEmpleado(criterio.getRfcEmpleado() + "%", pageable));
		} else if (!criterio.getIdPuestoNomb().isEmpty()) {
			//System.out.println("Por Puesto");
			//System.out.println(criterio.getIdPuestoNomb() + "%");
			model.addAttribute("empleados", repositoryPlaza.findByIdPuestoNomb(criterio.getIdPuestoNomb() + "%", pageable ));
		} else if (!criterio.getIdUnidadNomb().isEmpty()) {
			//System.out.println("Por Unidad");
			//System.out.println(criterio.getIdUnidadNomb());
			model.addAttribute("empleados", repositoryPlaza.findByIdUnidadNomb(criterio.getIdUnidadNomb(), pageable));
		}
		//model.addAttribute("empleados", repository.findByRfcEmpleado(criterio.getRfcEmpleado() + "%"));
		return "listaEmpleados";
	}

	@RequestMapping("/muestraCalculo")
	public String muestraCalculo(@ModelAttribute("empleado") String rfc, Model model ) {
		//System.out.println("Calculo para :");
		//System.out.println(rfc);
		List<Pago> emp = repositoryPago.findByRfcEmpleado(rfc);
		if(calculoPago.evalua(emp.get(0))) {
			model.addAttribute("pago", calculoPago.getPago());
			return "calculoNomina";
		}
		model.addAttribute("empleado", rfc);
		return "errorCalculo";
	}

}
