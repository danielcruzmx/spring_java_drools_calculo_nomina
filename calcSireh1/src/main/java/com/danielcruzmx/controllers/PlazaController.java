package com.danielcruzmx.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.danielcruzmx.repositories.PlazaRepo;

@Controller
public class PlazaController {
	
	private PlazaRepo plazaRepository;
	
	public PlazaController(PlazaRepo plazaRepository) {
		this.plazaRepository = plazaRepository;
	}
	
	@GetMapping("/admin/plazas")
    public String plazasPage(HttpServletRequest request, Model model) {
        
        int page = 0; //default page number is 0 (yes it is weird)
        int size = 10; //default page size is 10
        
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        
        model.addAttribute("plazas", plazaRepository.findAll(PageRequest.of(page, size)));
        return "plazas";
    }

}
